#!/bin/bash

service apache2 start
service zabbix-server start
service zabbix-agent start

#mysql -uroot -proot -e "CREATE DATABASE zabbix character set utf8 collate utf8_bin;" && \
#mysql -uroot -proot -e "CREATE USER 'zabbix'@'localhost' IDENTIFIED BY 'password';" && \
#mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'localhost' WITH GRANT OPTION;" && \
#mysql -uroot -proot -e "FLUSH PRIVILEGES;"

zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -uzabbix -ppassword zabbix

exec "$@"