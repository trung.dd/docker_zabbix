FROM ubuntu:16.04

RUN apt-get update

RUN apt-get install -y wget php php-mbstring php-gd php-xml php-bcmath php-ldap php-mysql apache2 libapache2-mod-php

RUN wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+xenial_all.deb
RUN	dpkg -i zabbix-release_4.0-3+xenial_all.deb
RUN	apt-get update
RUN	apt-get install -y zabbix-server-mysql zabbix-frontend-php zabbix-agent

COPY zabbix_server.conf /etc/zabbix/zabbix_server.conf
COPY zabbix.conf /etc/httpd/conf.d/zabbix.conf
COPY php.ini /etc/php/7.0/apache2/php.ini

WORKDIR /var/lib/zabbix

COPY start.sh /var/lib/zabbix/start.sh
RUN chmod +x /var/lib/zabbix/start.sh
ENTRYPOINT ["/var/lib/zabbix/start.sh"]

EXPOSE 80 10050 10051

CMD ["/var/lib/zabbix/start.sh"]